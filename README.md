# Domain Name Regex

A portable regular expression that matches domain names according to the DNS RFC.

## Demo

http://www.regexr.com/3e8n2

## The Expression

    ^((?:([a-z0-9]\.|[a-z0-9][a-z0-9\-]{0,61}[a-z0-9])\.)+)([a-z0-9]{2,63}|(?:[a-z0-9][a-z0-9\-]{0,61}[a-z0-9]))\.?$

You should enable the following flags:

* Ignore case (i; required)
* Global (g)
* Multiline (m)

## Limitations

The regex requires a TLD to be present.

## Features

* Simple and quickly comprehensible
* Portable
* Caputure Groups:
  * `$1` is always all subdomains + domain
  * `$2` is always main domain
  * `$3` is always TLD
* Allows all theoretically valid TLDs
  * That includes internal atrificial TLDs like *.internal*
* Supports Punycode in domains, subdomains and TLDs
* Checks max. length of each segment
* Checks min. length of TLD

## Hints

To search for domains in a text using this regex, change "^" and "$" to "\s"

## License

[WTFPL](http://www.wtfpl.net)